
## 0.0.12 [07-12-2024]

Add deprecation notice and metadata

See merge request itentialopensource/pre-built-automations/kafka-consumer!14

2024-07-12 20:15:38 +0000

---

## 0.0.11 [07-06-2023]

* update master to 2023.1

See merge request itentialopensource/pre-built-automations/kafka-consumer!13

---

## 0.0.10 [05-30-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/kafka-consumer!10

---

## 0.0.9 [06-28-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/kafka-consumer!9

---

## 0.0.8 [06-21-2022]

* DSUP-1291

See merge request itentialopensource/pre-built-automations/kafka-consumer!8

---

## 0.0.7 [12-03-2021]

* certified for 2021.2

See merge request itentialopensource/pre-built-automations/kafka-consumer!6

---

## 0.0.6 [09-20-2021]

* Certifying pre built for 2021.1

See merge request itentialopensource/pre-built-automations/kafka-consumer!5

---

## 0.0.5 [09-20-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/kafka-consumer!4

---

## 0.0.4 [11-17-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/kafka-consumer!2

---

## 0.0.3 [11-11-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/kafka-consumer!1

---

## 0.0.2 [11-11-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
